import fileinput
import re
import heapq
from functools import reduce
import copy

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

    nums = list(map(int, input()))
    nums.sort(reverse=True)
    s = sum(nums)

    w_limit = s//3

    def combs(acc, cur, csum, rnums, limit, n_limit):
        if not rnums:
            if csum == limit:
                acc.append(cur)
            return
        if csum > limit:
            return
        if len(cur) > n_limit:
            return
        if csum == limit:
            acc.append(cur)
            return
        combs(acc, cur, csum, rnums[1:], limit, n_limit)
        combs(acc, cur + [rnums[0]], csum + rnums[0], rnums[1:], limit, n_limit)

    ming = 100
    mintang = 10e10

    def verify(csum, rnums, limit):
        if not rnums:
            return csum == limit
        if csum > limit:
            return False
        if csum == limit:
            return True
        return verify(csum, rnums[1:], limit) or verify(csum + rnums[0], rnums[1:], limit)

    def search(cur, csum, cnum, cprod, rnums, nums, limit, n_limit):
        if cnum > n_limit:
            return 2e20
        if csum > limit:
            return 3e20
        if csum == limit:
            if verify(0, list(set(nums) - set(cur)), limit):
                return cprod
            else:
                return 4e20
        if not rnums:
            return 1e20
        p1 = search(cur + [rnums[0]], csum + rnums[0], cnum+1, cprod*rnums[0], rnums[1:], nums, limit, n_limit)
        p2 = search(cur, csum, cnum, cprod, rnums[1:], nums, limit, n_limit)
        return min(p1, p2)

    #acc = []
    #combs(acc, [], 0, nums, w_limit, 9)
    print('Num: ', len(nums), ', third: ', w_limit)
    for n_limit in range(len(nums)):
        q = search([], 0, 0, 1, nums, nums, w_limit, n_limit)
        if q < 1e20:
            print('Group size %d has entanglement %d' % (n_limit, q))
            break
        else:
            print('No group found for size %d' % n_limit)

    # part2
    w_limit = sum(nums)//4
    for n_limit in range(len(nums)):
        q = search([], 0, 0, 1, nums, nums, w_limit, n_limit)
        if q < 1e20:
            print('Group size %d has entanglement %d' % (n_limit, q))
            break
        else:
            print('No group found for size %d' % n_limit)


run()
