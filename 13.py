import fileinput
import re

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

    gainmap = {}

    r = re.compile(r'([A-Za-z]+) would (lose|gain) (\d+).*to ([A-Za-z]+).')
    for l in input():
        m = r.match(l)
        print(m[1], m[2], m[3], m[4])
        assert m, 'Could not match %s' % l
        gainee = m[1]
        sign = -1 if m[2] == 'lose' else 1
        units = int(m[3])
        neighbor = m[4]

        g = gainmap.get(gainee, {})
        g[neighbor] = units * sign
        gainmap[gainee] = g

    names = set(gainmap.keys())

    part2 = True

    if part2:
        gainmap['Me'] = {n: 0 for n in names}
        for n in names:
            gainmap[n]['Me'] = 0
        names.add('Me')

    def combs(cur, vals):
        if not vals:
            ps = zip(cur[:-1], cur[1:])
            r = sum(map(lambda a: gainmap[a[0]][a[1]] + gainmap[a[1]][a[0]], ps))
            r += gainmap[cur[0]][cur[-1]] + gainmap[cur[-1]][cur[0]]
            return r
        mx = 0
        for v in vals:
            m = combs(cur + [v], vals - {v})
            if m > mx:
                mx = m
        
        return mx

    print(combs([], names))
run()
