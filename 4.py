import fileinput
import md5

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():
	l = input()[0]

	ix = 1
	max = 1e8
	m = md5.new(l)
	while ix < max:
		mc = m.copy()
		mc.update(str(ix))
		a = mc.hexdigest()
		if a[:6] == '000000':
			print ix
			break
		ix = ix + 1

run()
