import fileinput
import re
import heapq
from functools import reduce

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():
    w = {
            'Dagger': (8, 4, 0),
            'Shortsword': (10, 5, 0),
            'Warhammer': (25, 6, 0),
            'Longsword': (40, 7, 0),
            'Greataxe': (74, 8, 0),
    }

    a = {
            'Leather':(13,0,1),
            'Chainmail':(31,0,2),
            'Splintmail':(53,0,3),
            'Bandedmail':(75,0,4),
            'Platemail':(102,0,5),
    }

    r = {
            'Damage +1':(25,1,0),
            'Damage +2':(50,2,0),
            'Damage +3':(100,3,0),
            'Defense +1':(20,0,1),
            'Defense +2':(40,0,2),
            'Defense +3':(80,0,3),
    }

    # generate combinations of up to num elements from ls
    # adds them all to acc
    def combs(acc, cur, ls, inv, num):
        if not ls:
            return
        if len(cur) >= num:
            return
        # only add the next element, done
        acc.append(cur + [inv[ls[0]]])
        # branch without the next element
        combs(acc, cur, ls[1:], inv, num)
        # branch with the next element
        combs(acc, cur + [inv[ls[0]]], ls[1:], inv, num)

    wacc = []
    combs(wacc, [], list(w.keys()), w, 1)
    aacc = [[]]
    combs(aacc, [], list(a.keys()), a, 1)
    racc = [[]]
    combs(racc, [], list(r.keys()), r, 2)

    gi = {}

    for l in input():
        ps = l.split(': ')
        gi[ps[0]] = int(ps[1])

    def playerwins(gi, dam, ar):
        # monster stats
        mh = gi['Hit Points']
        md = gi['Damage']
        ma = gi['Armor']

        # player health
        ph = 100

        while True:
            # player
            d = max(1, dam - ma)
            mh -= d
            if mh <= 0:
                return True
            # monster
            d = max(1, md - ar)
            ph -= d
            if ph <=0:
                return False


    ming = 10e6
    maxg = -1
    for w in wacc:
        for a in aacc:
            for r in racc:
                (g, d, ar) = reduce(lambda a, b: (a[0] + b[0], a[1] + b[1], a[2] + b[2]), w + a + r)
                if playerwins(gi, d, ar):
                    ming = min(ming, g)
                else:
                    maxg = max(maxg, g)

    print(ming, maxg)

run()
