import fileinput

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

    dsts = {}

    for l in input():
        ls = l.split(" ")
        src = ls[0]
        dst = ls[2]
        dist = ls[4]
        e = dsts.get(src, {})
        e.update({dst: int(dist)})
        dsts[src] = e
        e = dsts.get(dst, {})
        e.update({src: int(dist)})
        dsts[dst] = e

    def find_min(dsts, c, cs, fn, i_min_dist, dist):
        if not cs:
            return dist
        min_dist = i_min_dist
        for nc in cs:
            ddist = dsts[c][nc] if c else 0
            m = find_min(dsts, nc, cs - {nc}, fn, i_min_dist, dist + ddist)
            min_dist = fn(m, min_dist)
        return min_dist

    md = find_min(dsts, None, dsts.keys(), min, 1e10, 0)
    print(md)
    md = find_min(dsts, None, dsts.keys(), max, 0, 0)
    print(md)

run()
