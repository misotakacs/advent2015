import fileinput
import re
import heapq
from functools import reduce
import copy

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

    spells = {
            'Magic Missile': {
                'cost': 53,
                'bosshp': -4,
                'turns': 1,
            },
            'Drain': {
                'cost': 53,
                'bosshp': -2,
                'hp': 2,
                'turns': 1,
            },
            'Shield': {
                'cost': 113,
                'armor': 7,
                'turns': 6,
                'static': True,
            },
            'Poison': {
                'cost': 173,
                'bosshp': -3,
                'turns': 6,
            },
            'Recharge': {
                'cost': 229,
                'mana': 101,
                'turns': 5,
            },
    }

    gi = {}

    for l in input():
        ps = l.split(': ')
        gi[ps[0]] = int(ps[1])

    game = {
            'hp': 50,
            'armor': 0,
            'mana': 500,
            'bosshp': gi['Hit Points'],
            'bossdamage': gi['Damage'],
            'effects': {},
            'manaspent': 0,
            }

    def apply_spell(g, s, m=1):
        for k, v in s.items():
            if k in ['cost', 'static', 'turns']:
                continue
            g[k] += v*m

    def apply_effects(g):
        new_effects = {}
        for n, e in g['effects'].items():
            if e['turns']:
                e['turns'] -= 1
            if 'static' in e:
                if e['turns'] == 0:
                    apply_spell(g, e, -1)
                else:
                    new_effects[n] = e
                continue
            apply_spell(g, e)
            if 'turns' in e and e['turns'] > 0:
                new_effects[n] = e
        g['effects'] = new_effects

    def rgame(start, part2=False):
        cnt = 0
        q = []
        heapq.heappush(q, (0, 0, 0, copy.deepcopy(start)))
        while q:
            (_, _, _, g) = heapq.heappop(q)
            if part2:
                g['hp'] -= 1
                if g['hp'] <= 0:
                    continue
            # resolve effects
            apply_effects(g)
            # player chooses spell
            for n, s in spells.items():
                ng = copy.deepcopy(g)
                s = copy.copy(s)
                if ng['mana'] >= s['cost'] and n not in ng['effects']:
                    ng['effects'][n] = s
                    ng['mana'] -= s['cost']
                    ng['manaspent'] += s['cost']
                    if 'static' in s:
                        apply_spell(ng, s)
                    # boss + effects
                    apply_effects(ng)
                    if ng['bosshp'] <= 0:
                        print('Boss dead, mana spent: %d' % ng['manaspent'])
                        return
                    dmg = max(1, ng['bossdamage'] - ng['armor'])
                    ng['hp'] -= dmg
                    if ng['hp'] > 0:
                        cnt += 1
                        heapq.heappush(q, (ng['manaspent'], ng['bosshp'], cnt, ng))
                            
    rgame(game)
    rgame(game, part2=True)
run()
