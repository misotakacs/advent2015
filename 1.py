import fileinput

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():
	i = input()
	l = i[0]

	pos = 1
	floor = 0

	for c in l:
		if c == '(':
			floor = floor + 1
		else:
			floor = floor - 1
		if floor == -1:
			print pos
			break
		pos = pos + 1

run()
