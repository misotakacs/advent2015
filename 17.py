import fileinput
import re

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

    nums = [int(l) for l in input()]
    sums = [sum(nums[i:]) for i in range(len(nums))]

    target = 150

    counts = {i:0 for i in range(len(nums))}

    def check(seq, nums, sums, rest):
        if rest < 0:
            return 0
        if rest == 0:
            counts[len(seq)] += 1
            return 1
        if not nums:
            return 0
        # optimization
        if sums[0] < rest:
            return 0
        s = 0
        # using current jug
        s += check(seq + [nums[0]], nums[1:], sums[1:], rest - nums[0])
        # skipping it
        s += check(seq, nums[1:], sums[1:], rest)
        return s

    print(check([], nums, sums, target))
    print(counts)

run()
