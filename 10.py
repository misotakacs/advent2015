import fileinput

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

	i = "1113122113"

	def expand(s):
		ret = ""
		prev = None
		cnt = 0
		
		for c in s:
			if c == prev:
				cnt += 1
				continue
			else:				
				if prev:
					ret += str(cnt) + prev
				prev = c
				cnt = 1

		if cnt > 0:
			ret += str(cnt) + prev

		return ret
	
	e = expand("1")
	assert e == "11", "supposed: 11, was: %s" % e
	assert expand("11") == "21"
	assert expand("21") == "1211"

	for _ in range(50):
		i = expand(i)
		print(len(i))

run()
