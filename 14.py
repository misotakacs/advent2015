import fileinput
import re

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

    deer = []

    r = re.compile(r'.* (\d+) km/s for (\d+).*rest for (\d+) seconds.')
    for l in input():
        m = r.match(l)
        assert m[1] and m[2] and m[3], "Cannot parse %s" % l

        deer.append((int(m[1]), int(m[2]), int(m[3])))

    seconds = 2503
    maxd = -1

    points = [0 for d in deer]

    part2 = True

    for s in range(1 if part2 else seconds, seconds+1):
        dsts = [0 for d in deer]
        maxd = -1
        for ix, d in enumerate(deer):
            period = d[1] + d[2]
            it = s // period
            rest = s % period

            dist = it * d[0] * d[1] + min(rest, d[1])*d[0]

            maxd = max(maxd, dist)

            dsts[ix] = dist

        for ix, d in enumerate(dsts):
            if d == maxd:
                points[ix] += 1

    print(maxd)
    print(max(points))
        

run()
