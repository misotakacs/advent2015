import fileinput
import re
import heapq
from functools import reduce
import copy

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

    r = re.compile(r'(hlf|tpl|inc|jmp|jie|jio) ([-+a-z0-9]+),? ?([-+0-9]*)$')

    program = []
    for l in input():
        m = r.match(l)
        assert m, 'Cannot match %s' % l
        program.append((m[1], m[2], m[3] if m[3] else None))

    def run(a, b):

        regs = {'a':a, 'b':b}

        pc = 0
        while pc >= 0 and pc < len(program):
            (ins, p1, p2) = program[pc]
            print(ins, p1, p2)
            if ins == 'hlf':
                regs[p1] >>= 1
                pc += 1
            elif ins == 'tpl':
                regs[p1] *= 3
                pc += 1
            elif ins == 'inc':
                regs[p1] += 1
                pc += 1
            elif ins == 'jmp':
                pc += int(p1)
            elif ins == 'jie':
                if regs[p1] % 2 == 0:
                    pc += int(p2)
                else:
                    pc += 1
            elif ins == 'jio':
                if regs[p1] == 1:
                    pc += int(p2)
                else:
                    pc += 1

        return regs


    print(run(0, 0)['b'])
    print(run(1, 0)['b'])

run()
