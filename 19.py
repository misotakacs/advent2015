import fileinput
import re
import heapq

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():
    ls = input()

    code = ls[-1]
    ls = ls[:-2]

    rules = {}
    revrules = {}
    for l in ls:
        ps = l.split(' => ')
        r = rules.get(ps[0], [])
        r.append(ps[1])
        rules[ps[0]] = r
        revrules[ps[1]] = ps[0]

    gen = set()

    prefix = ''

    def opts(start):
        gen = set()
        for ix in range(len(start)):
            gen = gen.union({start[:ix] + r + start[ix+1:] for r in rules.get(start[ix], [])})
            if ix < len(start) - 1:
                gen = gen.union({start[:ix] + r + start[ix+2:] for r in rules.get(start[ix:ix+2], [])})
        return gen
     

    print(len(opts(code)))

    # part2

    skeys = sorted(revrules.keys(), key=len, reverse=True)

    def revopts(word):
        gen = set()
        for i in range(0, len(word)):
            for k in skeys:
                ix = word.find(k, i)
                if ix is not None:
                    gen = gen.union({word[:ix] + revrules[k] + word[ix+len(k):]})
        return gen

    q = []
    heapq.heappush(q, (len(code), (code, 0)))
    vis = set(code)

    while True:
        (l, (s, d)) = heapq.heappop(q)
        if s == 'e':
            print(d)
            break
        for o in revopts(s):
            if o not in vis:
                vis.add(o)
                heapq.heappush(q, (len(o), (o, d+1)))



run()
