import fileinput
import re

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

    r = re.compile(r'(.*): capacity (-?\d+), durability (-?\d+), flavor (-?\d+), texture (-?\d+), calories (-?\d+)')

    ing = []
    for l in input():
        m = r.match(l)
        ing.append((m[1], int(m[2]), int(m[3]), int(m[4]), int(m[5]), int(m[6])))

    part2 = True

    def calc(ingix, sumw, parts, ws, maxprop):
        if ingix == len(ing):
            prod = 1
            if part2 and parts[4] != 500:
                return 0
            for ix in range(4):
                if parts[ix] <= 0:
                    return 0
                prod *= parts[ix]
            return prod

        mc = 0
        for w in range(0, 100-sumw+1):
            if ingix == 0:
                print(w)
            ps = [0 for p in parts]
            for pix in range(maxprop):
                ps[pix] = parts[pix] + w*ing[ingix][pix+1]
            mc = max(mc, calc(ingix+1, sumw+w, ps, ws + [w], maxprop))

        return mc

    print(calc(0, 0, [0 for _ in range(5)], [], 5))

run()
