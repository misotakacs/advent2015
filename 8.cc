#include <iostream>
#include <string>
#include <cassert> 
#include <sstream>

struct counts {
    int code;
    int memory;
};

using namespace std;

string escape(std::string &str) {
    stringstream ss;
    ss << '"';
    for (char const &c: str) {
        if (c == '"' || c == '\\') {
            ss << '\\';
        }
        ss << c;
    }

    ss << '"';
    return ss.str();
}

counts cnt(std::string &str) {
    counts count = {1, 0};
    assert(str[0] == '"');
    bool escape = false;
    for (int i = 1; i < str.length(); i++) {
        char c = str[i];
        if (!escape) {
            if (c == '"') {
                count.code += 1;
                return count;
            }
            if (c == '\\') {
                count.code += 1;
                escape = true;
                continue;
            }
            count.code += 1;
            count.memory += 1;
        } else {
            if (c == '"' || c == '\\') {
                count.code += 1;
                count.memory += 1;
                escape = false;
                continue;
            }
            if (c == 'x') {
                count.code += 3;
                count.memory += 1;
                escape = false;
                i += 2;
                continue;
            }
            assert(false);
        }
    }
    assert(false);
}

int main() {
    counts cnts = {0, 0};
    int total = 0;
    for (std::string line; std::getline(std::cin, line);) {
        auto c = cnt(line);
        cout << line << "gives " << c.code << ", " << c.memory << endl;
        cnts.code += c.code;
        cnts.memory += c.memory;
        auto s = escape(line);
        auto c2 = cnt(s);
        total += c2.code;
    }

    cout << "Result: " << cnts.code - cnts.memory << endl;
    cout << "Result2: " << total - cnts.code << endl;

    return 0;
}
