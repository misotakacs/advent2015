import fileinput

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():
    bad = ['ab', 'cd', 'pq', 'xy']
    wov = 'aeiou'
    nice_cnt1 = 0
    nice_cnt2 = 0

    def isnice1(l):
        wovs = int(l[0] in wov)
        prev = l[0]
        dbl = False
        for ix in range(1, len(l)):            
            if l[ix] == prev:
                dbl = True
            wovs += int(l[ix] in wov)
            if (prev + l[ix]) in bad:
                return False
            prev = l[ix]

        return dbl and wovs >= 3

    assert isnice1('ugknbfddgicrmopn')
    assert isnice1('aaa')
    assert not isnice1('jchzalrnumimnmhp')
    assert not isnice1('haegwjzuvuyypxyu')
    assert not isnice1('dvszwmarrgswjxmb')

    def isnice2(l):
        dbl = {}
        rep = False
        dblf = False
        for ix, c in enumerate(l):
            if ix > 1 and l[ix-2] == c:
                rep = True
            if ix > 0:
                ss = l[ix-1] + c
                if ss not in dbl:
                    dbl[ss] = ix
                else:
                    if dbl[ss] < ix - 1:
                        dblf = True

        return dblf and rep

    assert isnice2('qjhvhtzxzqqjkmpb')
    assert isnice2('xxyxx')
    assert not isnice2('uurcxstgmygtbstg')
    assert not isnice2('ieodomkazucvgmuy')
    
    nice_cnt2 = 0

    for l in input():
        nice_cnt1 += int(isnice1(l))
        nice_cnt2 += int(isnice2(l))
   
    print(nice_cnt1, nice_cnt2)
        

run()


