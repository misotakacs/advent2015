import fileinput

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

D = {
	'v': (0, 1),
	'^': (0, -1),
	'<': (-1, 0),
	'>': (1, 0),
}

def run():
	l = input()[0]

	visited = dict()
	ps = [(0, 0), (0, 0)]
	ap = 0
	for c in l:
		p = ps[ap]
		visited['%d,%d' % p] = True
		d = D[c]
		p = (p[0] + d[0], p[1] + d[1])
		ps[ap] = p
		visited['%d,%d' %p] = True
		ap = (ap + 1) % len(ps)

	print len(visited)

run()
