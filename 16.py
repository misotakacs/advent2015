import fileinput
import re

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

    comps = {
        'children': 3,
        'cats': 7,
        'samoyeds': 2,
        'pomeranians': 3,
        'akitas': 0,
        'vizslas': 0,
        'goldfish': 5,
        'trees': 3,
        'cars': 2,
        'perfumes': 1,
    }

    possible = []

    part2 = True

    r = re.compile(r'Sue (\d+): (.+)$')
    for l in input():
        m = r.match(l)
        assert m, "Could not parse %s" % l
        ps = m[2].split(', ')
        skip = False
        for p in ps:
            cs = p.split(': ')
            comp = cs[0]
            val = int(cs[1])
            if not part2 and comps[comp] != val:
                skip = True
                break
            if part2:
                print(m[1])
                if comp in ['cats', 'trees']:
                    if val <= comps[comp]:
                        skip = True
                        break
                elif comp in ['pomeranians', 'goldfish']:
                    if val >= comps[comp]:
                        skip = True
                        break
                elif comps[comp] != val:
                    skip = True
                    break

        if skip:
            print(comp, val, comps[comp])
            continue
        possible.append(int(m[1]))

    print(possible)



run()
