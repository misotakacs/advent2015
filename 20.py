import math

def run():

    cnt = 36000000
    x = cnt // 10

    hs = {}

    mn = 10e6

    for i in range(1,x):
        s = i
        if s % 10000 == 0:
            print(s)
        while s < x:
            hs[s] = hs.get(s, 0) + 10*i
            if hs[s] >= cnt:
                mn = min(mn, s)
            s += i

    print(mn)

    # part2

    mn = 10e8
    x = cnt // 11
    hs = {}

    for i in range(1,x):
        s = i
        if s % 10000 == 0:
            print(s)
        it = 50
        while s < x and it > 0:
            hs[s] = hs.get(s, 0) + 11*i
            if hs[s] >= cnt:
                mn = min(mn, s)
            s += i
            it -= 1

    print(mn)


run()
