import fileinput

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():
	i = input()

	r = 0

	for l in i:
		dim = l.split('x')
		assert len(dim) == 3, "%s did not split into 3 parts" % l
		print dim
		w = int(dim[0])
		h = int(dim[1])
		d = int(dim[2])
		sides = [2*w + 2*h, 2*w + 2*d, 2*h + 2*d]
		minside = min(sides)
		r = r + w*h*d + minside

	print r

run()
