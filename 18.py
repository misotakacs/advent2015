import fileinput
import re

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

    def neigbours(x, y, maxx, maxy):
        for cx in range(max(0, x-1), min(x+2, maxx)):
            for cy in range(max(0,y-1), min(y+2, maxy)):
                if cx == x and cy == y:
                    continue
                yield (cx, cy)


    f = [l for l in input()]
    for _ in range(100):
        nf = []
        for y, l in enumerate(f):
            nl = ''
            for x, c in enumerate(l):
                if (x, y) in [(0, 0), (len(f)-1, 0), (0, len(l)-1), (len(f)-1, len(l)-1)]:
                    nl += '#'
                    continue
                cnt = 0
                for n in neigbours(x, y, len(l), len(f)):
                    if f[n[1]][n[0]] == '#':
                        cnt += 1
                rc = c
                if c == '#' and cnt not in [2,3]:
                    rc = '.'
                elif c == '.' and cnt == 3:
                    rc = '#'
                nl += rc
            nf.append(nl)
        f = nf

    print(sum([len(list(filter(lambda x: x == '#', l))) for l in f]))

run()
