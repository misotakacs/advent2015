import fileinput

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

class val():
    STR = 1
    INT = 2
    ARR = 3
    OBJ = 4

    def __init__(self, t, val=None):
        self.val = val
        self.type = t

    def enumerate(self):
        if self.type not in [self.ARR, self.OBJ]:
            raise ValueError("Type %d cannot enumerate" % self.type)
        for val in self.val:
                yield val

    def __str__(self):
        if self.type == val.ARR:
            return '[' + ','.join(map(str, self.enumerate())) + ']'
        elif self.type == val.OBJ:
            return '{' + ','.join(map(lambda x: str(x[0]) + ':' + str(x[1]), self.enumerate())) + '}'
        elif self.type == val.STR:
            return '"' + self.val + '"'
        elif self.type == val.INT:
            return str(self.val)
        else:
            raise ValueError('Unknown type: %s' % self.type)


def parseInt(s):
    i = 0
    m = 1
    for ix, c in enumerate(s):
        if c not in '-0123456789':
            return val(val.INT, i*m), s[ix:]
        if c == '-':
            m = -1
            continue
        i = i*10 + int(c)

    raise ValueError('parseInt: Should not happen')

def parseStr(s):
    rs = ""
    for ix, c in enumerate(s):
        if c == '"':
            return val(val.STR, rs), s[ix+1:]
        rs += c

    raise ValueError('parsestr: ran out of string')

def parseArr(s):
    a = val(val.ARR, [])
    while True:
        if s[0] == ']':
            return a, s[1:]
        if s[0] == ',':
            s = s[1:]
            continue
        v, s = parseOnChar(s)
        a.val.append(v)

def parseObj(s):
    o = val(val.OBJ, [])
    k, v = None, None
    while True:
        if s[0] == '}':
            return o, s[1:]
        if s[0] == ',':
            s = s[1:]
            continue
        k, s = parseOnChar(s)
        assert s[0] == ':', 'Expecting :, but found %s' % s[:10]
        s = s[1:]
        v, s = parseOnChar(s)
        o.val.append((k, v))

def parseOnChar(s):
    c = s[0]
    if c == '[':
        return parseArr(s[1:])
    if c == '{':
        return parseObj(s[1:])
    if c == '"':
        return parseStr(s[1:])
    if c in '-0123456789':
        return parseInt(s)
    raise ValueError('parseOnChar: Invalid character %s' % s[0])

def run():

    i = input()
    p, s = parseOnChar(i[0])

    part2 = True

    def rsum(val):
        if val.type == val.INT:
            return val.val
        if val.type == val.ARR:
            return sum([rsum(c) for c in val.enumerate()])
        if val.type == val.OBJ:
            if part2:
                if any(filter(lambda c: c[1].type == val.STR and c[1].val == 'red', val.enumerate())):
                    return 0
            return sum([rsum(c[0]) + rsum(c[1]) for c in val.enumerate()])
        return 0
                
    print(rsum(p))

run()
