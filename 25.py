import fileinput
import re
import heapq
from functools import reduce
import copy

def input():
	i = []
	for line in fileinput.input():
		i.append(line.rstrip())
	return i

def run():

  r = re.compile('.*row (\d+), column (\d+)\.$')
  m = r.match(input()[0])
  assert m

  tr = int(m[1])
  tc = int(m[2])

  print(tr,tc)

  cur = 20151125
  mul = 252533
  div = 33554393

  r, c = 1, 1

  while True:
      cur *= mul
      cur %= div
      if r == 1:
          r = c+1
          c = 1
      else:
          r -= 1
          c += 1
      if r == tr and c == tc:
          print(cur)
          break

run()
